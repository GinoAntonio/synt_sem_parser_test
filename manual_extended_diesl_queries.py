import random

from collections import namedtuple

# 100% A - Alle queries 50 50
OpenTableQuery = namedtuple("OpenTableQuery", ["intent", "snippet"])


manualDieslQueries = [
    OpenTableQuery("trim whitespace right", "db.student.name = db.student.name.trim(right)"),
    OpenTableQuery("trim whitespace on the right of the column name", "db.student.name = db.student.name.trim(right)"),
    OpenTableQuery("Trim whitespace left", "db.student.name = db.student.name.trim(left)"),
    OpenTableQuery("trim whitespaces on both sides", "db.student.name = db.student.name.trim(both)"),
    OpenTableQuery("Trim the whitespace on both sides", "db.student.name = db.student.name.trim(both)"),

    OpenTableQuery('"Hello" to operation',  '"Hello".toOperation'),
    OpenTableQuery("make 'hello' to operation", '"hello".toOperation'),
    OpenTableQuery("convert 'this' to Operation", '"this".toOperation'),
    OpenTableQuery("make this to operation", '"this".toOperation'),

    OpenTableQuery("Extract all characters from the second to the fifth", 'db.students.name = db.students.name[2..5]'),
    OpenTableQuery("Extracts all characters between the third and sixth points",
                   'db.students.name = db.students.name[2..5]'),

    OpenTableQuery('to upper this', 'db.student.new = db.student.old.toUpper()'),
    OpenTableQuery('to uppercase this', 'db.student.new = db.student.old.toUpper()'),
    OpenTableQuery('make this to uppercase', 'db.student.new = db.student.old.toUpper()'),
    OpenTableQuery('convert "word" to lower', 'db.student.new = db.student.old.toLower()'),
    OpenTableQuery('write "word" BIG', 'db.student.new = db.student.old.toUpper()'),

    OpenTableQuery('replace "hello" for "word"',
                   'db.student.new. = db.student.old.replace("hello".toOperation, "word".toOperation)'),
    OpenTableQuery('replace hello for "word"',
                   'db.student.new. = db.student.old.replace("hello".toOperation, "word".toOperation)'),
    OpenTableQuery('replace hello for word',
                   'db.student.new. = db.student.old.replace("hello".toOperation, "word".toOperation)'),

]
