from example import Example

# 100% A

ot_train_examples = [
    Example(input='replace a,b', semantics='replace(a,b)'),
]

ot_test_examples = [
    Example(input='replace a with b', semantics='replace(a,b)'),
]