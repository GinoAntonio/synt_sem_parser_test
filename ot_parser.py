from collections import defaultdict
from itertools import product
from types import FunctionType
from collections.abc import Iterable

from rule import Rule

MAX_CELL_CAPACITY = 10000
STOPPWORDS = ['in', 'to', 'with', 'of', 'for']

# TODO: is_cat and is_lexical are defined twice, fix more elegantly


def is_cat(label):
    return label.startswith('$')


def split(word):
    return [char for char in word]

# A
def between_characters(string, start_character, end_character):
    """ Looks for a string in between two characters.
    Returns the substring in between characters and the position where the end character was found,
     if the substring was not found it returns 0 """
    substring = ""
    looking_for_substring = False
    for iteration, character in enumerate(split(string)):
        # character is = character, and not searching => start searching
        if (character == start_character) & (looking_for_substring is False) & (substring == ""):
            looking_for_substring = True
        # Searching and we did not find the end of the search
        if (character != end_character) & looking_for_substring:
            substring = substring + character
        # found end_character? => stop search
        if (substring != "") & (character == end_character) & looking_for_substring is True:
            final_position = iteration + len(end_character)
            return substring, final_position
    return 0


# A
def find_all(string, start_character, end_character):
    """ Finds the substrings in a string between characters.
    After the first match it continues looking for substrings after the last character of a match.
    Returns a list with the found substrings"""
    string_end = 0
    substring_list = []
    while string[0:string_end] is not string:
        if between_characters(string[string_end:], start_character, end_character) == 0:
            return substring_list
        substring_list.append(between_characters(string[string_end:], start_character, end_character)[0])
        string_end = string_end + between_characters(string[string_end:], start_character, end_character)[1]
    return substring_list


def whitespace(string):
    if ' ' in string:
        return True
    return False

# G
def is_lexical(rule):
    return all([not is_cat(rhsi) for rhsi in rule.rhs])


# Important for catching e.g. unary cycles.
def check_capacity(chart, i, j):
    if len(chart[(i, j)]) >= MAX_CELL_CAPACITY:
        print('Cell (%d, %d) has reached capacity %d' % (
            i, j, MAX_CELL_CAPACITY))
        return False
    return True

# G
def apply_lexical_rules(grammar, chart, tokens, i, j):
    """Add parses to span (i, j) in chart by applying lexical rules from grammar to tokens."""
    for rule in grammar.lexical_rules[tuple(tokens[i:j])]:
        chart[(i, j)].append(Parse(rule, tokens[i:j]))


def apply_unary_rules(grammar, chart, i, j):
    """Add parses to chart cell (i, j) by applying unary rules."""
    # Note that the last line of this method can add new parses to chart[(i,
    # j)], the list over which we are iterating.  Because of this, we
    # essentially get unary closure "for free".  (However, if the grammar
    # contains unary cycles, we'll get stuck in a loop, which is one reason for
    # check_capacity().)
    if hasattr(grammar, 'unary_rules'):
        for parse in chart[(i, j)]:
            for rule in grammar.unary_rules[(parse.rule.lhs,)]:
                if not check_capacity(chart, i, j):
                    return
                chart[(i, j)].append(Parse(rule, [parse]))


def apply_binary_rules(grammar, chart, i, j):
    """Add parses to span (i, j) in chart by applying binary rules from grammar."""
    for k in range(i + 1, j):  # all ways of splitting the span into two subspans
        for parse_1, parse_2 in product(chart[(i, k)], chart[(k, j)]):
            for rule in grammar.binary_rules[(parse_1.rule.lhs, parse_2.rule.lhs)]:
                chart[(i, j)].append(Parse(rule, [parse_1, parse_2]))


def apply_annotators(grammar, chart, tokens, i, j):
    """Add parses to chart cell (i, j) by applying annotators."""
    if hasattr(grammar, 'annotators'):
        for annotator in grammar.annotators:
            for category, semantics in annotator.annotate(tokens[i:j]):
                if not check_capacity(chart, i, j):
                    return
                rule = Rule(category, tuple(tokens[i:j]), semantics)
                chart[(i, j)].append(Parse(rule, tokens[i:j]))


def compute_semantics(parse):
    if is_lexical(parse.rule) or not isinstance(parse.rule.sem, FunctionType):
        return parse.rule.sem
    else:
        return parse.rule.sem([child.semantics for child in parse.children])


def filter_stopwords(eingabeText):
    out = [word for word in eingabeText if word not in STOPPWORDS]
    return out

# G
def parse_input(grammar, input):
    """Returns a list of all parses for input using grammar."""
    tokens = input.split()
    tokens = filter_stopwords(tokens)
    chart = defaultdict(list)
    for j in range(1, len(tokens) + 1):
        for i in range(j - 1, -1, -1):
            apply_annotators(grammar, chart, tokens, i, j)
            apply_lexical_rules(grammar, chart, tokens, i, j)
            apply_binary_rules(grammar, chart, i, j)
            apply_unary_rules(grammar, chart, i, j)
    parses = chart[(0, len(tokens))]
    if hasattr(grammar, 'start_symbol') and grammar.start_symbol:
        parses = [parse for parse in parses if parse.rule.lhs == grammar.start_symbol]
    return parses

# A
def parse_return_rules(grammar, queries):
    rules = []
    for example in queries:
        parses = parse_input(grammar, example.input)
        for parse in parses:
            print(parse.rule)
            # rules = rules.append(parse.rule)
    return rules


def parse_semantics(grammar, queries):
    semantics = []
    for example in queries:
        parses = parse_input(grammar, example.input)
        for parse in parses:
            t = parse.semantics
            if isinstance(t, Iterable):
                text = "".join(t)
                semantics.append(text)
            else:
                semantics.append(parse.semantics)
    return semantics

# A
def parse_semantic(grammar, example):
    semantics = []
    parses = parse_input(grammar, example.input)
    for parse in parses:
        t = parse.semantics
        if isinstance(t, Iterable):
            text = "".join(t)
            semantics.append(text)
        else:
            semantics.append(parse.semantics)
    return semantics

#G
def show_parse_trees(tb1_list, grammar):
    for example in tb1_list:
        parses = parse_input(grammar, example.input)
        print()
        print('%-16s %s' % ('input', example.input))
        if not parses:
            print(f"---no parses were found for this input: {example.input}")
        for idx, parse in enumerate(parses):
            print('%-16s %s' % ('parse %d' % idx, parse))
            print("--------parse ----")
            print(parse)
            print("---------semantic-----")
            print(parse.semantics)
            print("------Output----------")
            t = parse.semantics

            if isinstance(t, Iterable):
                text = "".join(t)
                print(text)
                print("\n")
                print("\n")
            else:
                print(t)

#G
def show_syntactic_tree(grammar, example):
    """Show all the syntactic trees for an input """

    parses = parse_input(grammar, example)
    print(f"input: {example}")
    for index, parse in enumerate(parses):
        print(f"parse number {index}: {parse}")


# G
class Parse:
    def __init__(self, rule, children):
        self.rule = rule
        self.children = tuple(children[:])
        self.semantics = compute_semantics(self)
        self.score = float('NaN')  # not now
        self.denotation = None  # not now

    def __str__(self):
        return '(%s %s)' % (self.rule.lhs, ' '.join([str(c) for c in self.children]))

    def ruleFeatures(self):
        """
        Create a dictionary from (string representations of) rules to how often they were
        used in the given parse.
        """

        def collectRuleFeatures(parse, features):
            feature = str(parse.rule)
            features[feature] += 1
            for child in parse.children:
                if isinstance(child, Parse):
                    collectRuleFeatures(child, features)

        features = defaultdict(int)
        collectRuleFeatures(self, features)

        return features
#R-sippy
    def rulePrecedenceFeatures(self):
        """
        Create a dictionary from rule pairs mapped to the count of their occurence.

        Rule pairs consists of two rules (A, B) where rule A is used directly after rule B
        in the parse tree, meaning that rule A is the corresponding rule to the node directly
        above the node of rule B.
        Rule pairs are represented as a tuple of their corresponding string representation.
        """

        def collectFeatures(parse, features):
            for child in parse.children:
                if isinstance(child, Parse):
                    features[(str(parse.rule), str(child.rule))] += 1
                    collectFeatures(child, features)

        features = defaultdict(float)
        collectFeatures(self, features)

        return features
# R-sippy
    def featureVector(self, grammar):
        """A feature vector of rule occurences used for scoring this parse."""
        ruleFeatureDict = grammar.ruleFeatureDict

        parseRuleFeatureDict = self.ruleFeatures()
        for ruleAsString, frequency in parseRuleFeatureDict.items():
            ruleFeatureDict[ruleAsString] += frequency

        parsePrecedenceRuleFeatureDict = self.rulePrecedenceFeatures()
        for ruleTuple, frequency in parsePrecedenceRuleFeatureDict.items():
            ruleFeatureDict[ruleTuple] += frequency

        return [frequency for ruleAsString, frequency in ruleFeatureDict.items()]
