from grammar import Grammar
from ot_parser import parse_semantic, parse_input, compute_semantics

from queries_loader import queries_maker
from makeRules import MakeRules
from new_rules import new_rules
from diesl_rules import diesl_rules
from diesl_queries_4_ml import manualDieslQueries

# 50%-50% Die main wurde sehr viel geändert...
tb1_grammar = Grammar(diesl_rules())

to_make_query = [query[0] for query in manualDieslQueries]
queries = [MakeRules(query) for query in to_make_query]
result = [query[1] for query in manualDieslQueries]

for entry in queries:
    semantics = parse_semantic(tb1_grammar, entry)
    print(semantics)
