# G-si
class Rule:
    def __init__(self, lhs, rhs, sem=None):
        self.lhs = lhs
        self.rhs = tuple(rhs.split()) if isinstance(rhs, str) else rhs
        self.sem = sem

    def __str__(self):
        return 'Rule' + str((self.lhs, ' '.join(self.rhs), self.sem))

