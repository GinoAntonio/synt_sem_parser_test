import csv
import os
import pathlib

from os import walk
from rule import Rule


def sems_0(sems):
    return sems[0]


def sems_1(sems):
    return sems[1]


def merge_dicts(d1, d2):
    if not d2:
        return d1
    result = d1.copy()
    result.update(d2)
    return result


def construct_rules_out_one_csv_file(csv_source):
    my_list_of_rules = []
    with open(csv_source, 'r') as f:
        reader = csv.reader(f, delimiter=';')
        for row in reader:
            if not row:
                continue
            if row[0].startswith('#'):
                continue
            if not row[2]:
                row[2] == ""
            if row[2].startswith("sem"):
                row[2] = eval(row[2])
            elif row[2].startswith("lamb"):
                row[2] = eval(row[2])
            my_list_of_rules.append(Rule(row[0], row[1], row[2]))
    return my_list_of_rules


# quelle = filename = os.path.join(pathlib.Path(__file__).parent, "a.csv")
# k = extraRules(quelle)

def collect_all_rules_from_csv_files():
    mypath = pathlib.Path(__file__).parent

    allFilesFromFolder = []
    for (dirpath, dirnames, filenames) in walk(mypath):
        allFilesFromFolder.extend(filenames)
        break

    only_CSV_files_from_folder = [filename for filename in allFilesFromFolder if filename.endswith(".csv")]
    collection_of_all_rules = []
    for folders in only_CSV_files_from_folder:
        filename = os.path.join(pathlib.Path(__file__).parent, folders)
        rules_from_folder = (construct_rules_out_one_csv_file(filename))
        for rule in rules_from_folder:
            collection_of_all_rules.append(rule)
    return collection_of_all_rules
