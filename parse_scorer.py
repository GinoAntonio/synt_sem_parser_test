from collections import defaultdict
from ot_parser import parse_input, Parse, show_parse_trees, parse_semantics
from grammar import Grammar
from diesl_rules import diesl_rules
from queries_loader import queries_maker
from rules_collection.accumulatorForRules import collect_all_rules_from_csv_files

# R-sippy
def rule_features(parse):
    """
    Returns a map from (string representations of) rules to how often they were
    used in the given parse.
    """

    def collect_rule_features(parse, features):
        feature = str(parse.rule)
        features[feature] += 1.0
        for child in parse.children:
            if isinstance(child, Parse):
                collect_rule_features(child, features)

    features = defaultdict(float)
    collect_rule_features(parse, features)
    return features

# R-sippy
def rule_precedence_features(parse):
    """
        Create a dictionary from rule pairs mapped to the count of their occurence.

        Rule pairs consists of two rules (A, B) where rule A is used directly after rule B
        in the parse tree, meaning that rule A is the corresponding rule to the node directly
        above the node of rule B.
        Rule pairs are represented as a tuple of their corresponding string representation.
        """

    def collectFeatures(parse, features):
        for child in parse.children:
            if isinstance(child, Parse):
                features[(str(parse.rule), str(child.rule))] += 1
                collectFeatures(child, features)

    features = defaultdict(float)
    collectFeatures(parse, features)

    return features


# R small changes A
def featureVector(parse, grammar):
    # TODO: this function is not hardcoded since, we are trying to access the key Rule($token, random_word)
    """A feature vector of rule occurences used for scoring this parse."""
    ruleFeatureDict = grammar.ruleFeatureDict

    parseRuleFeatureDict = rule_features(parse)
    # !Warning! This part is going to be hardcoded
    # We are taking away the tokens, since we cannot create a dictionary key
    # for all possible tokens.
    token = '$Token'
    for ruleAsString, frequency in parseRuleFeatureDict.items():
        if token not in str(ruleAsString):
            ruleFeatureDict[ruleAsString] += frequency

    parsePrecedenceRuleFeatureDict = rule_precedence_features(parse)
    for ruleTuple, frequency in parsePrecedenceRuleFeatureDict.items():
        if token not in str(ruleTuple):
            ruleFeatureDict[ruleTuple] += frequency

    return [frequency for ruleAsString, frequency in ruleFeatureDict.items()]

# A
def print_feature_vector(parses, grammar):
    for parse in parses:
        print(featureVector(parse, grammar))


#G
def print_rule_features(parses):
    # Prints how often what rule was used for each parse
    for parse in parses:
        for feature, value in rule_features(parse).items():
            print('%8g   %s' % (value, feature))
        print("____________rule_features________________________")
    print("===========================================================")


# G
def print_rule_precedence_features(parses):
    # Prints how many times a combination of rules has been used
    for parse in parses:
        for feature, value in rule_precedence_features(parse).items():
            print('%8g   %s' % (value, feature))
        print("............rule_precedence_features..........")
    print("===========================================================")

#A
from diesl_queries_4_ml import manualDieslQueries
tb1_grammar = Grammar(diesl_rules())

queries_diesl = (queries_maker("queries_diesl"))

# queries_diesl does not work for the printing the feature vectors
# queries_diesl = (queries_maker("queries_diesl"))
# print(tb1_grammar.ruleFeatureDict)

queries = [query[0] for query in manualDieslQueries]

for text in queries:
    parses = parse_input(tb1_grammar, text)
    for parse in parses:
        print_rule_features(parses)
    #print_feature_vector(parses, tb1_grammar)
