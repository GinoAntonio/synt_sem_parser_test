import csv
import os
from collections.abc import Iterable
from collections import defaultdict

from ot_parser import parse_input
from grammar import Grammar
from rules_collection.accumulatorForRules import collect_all_rules_from_csv_files

# 70%G, 30A
def get_path(folder, filename):
    # current_file = os.path.abspath(os.path.dirname(__file__))
    current_file =  os.path.join(os.path.dirname(__file__), folder, filename)
    # current_file = os.path.join(os.path.dirname(__file__), filename)
    return current_file


def get_testset(folder, filename):
    testsetpath = (get_path(folder, filename))
    testdata = []
    my_list_of_testcases = []  # each entry has input and the matching semantic resolution
    with open(testsetpath, 'r') as f:
        reader = csv.reader(f, delimiter=';')
        for row in reader:
            if not row:
                continue
            if row[0].startswith('#'):
                continue
            testdata.append(row)
        return testdata


def check_testset(folder, filname):
    """ Evaluates the grammar, and returns
           -semantics accuracy: percentage of how many 0th parses of each example match the expected semantics
           -semantics oracle accuracy: percentage of if least one parse matched the expected semantics
    """
    testset = get_testset(folder, filname)
    number_of_inputs = 0
    number_of_correct_parse_on_first_place = 0
    number_of_correct_parses = 0

    for row in testset:
        number_of_inputs += 1
        parses = parse_input(tb1_grammar, row[0])
        dict_of_numerated_parse_results = {}
        dict_of_numerated_parse_results[0] = ""
        for idx, parse in enumerate(parses):
            # print("parse " + str(idx))
            # print("--------parse ----")
            # print(parse)
            # print("---------semantic-----")
            # print(parse.semantics)
            # print("------Output----------")
            t = parse.semantics
            if isinstance(t, Iterable):
                text = "".join(t)
            dict_of_numerated_parse_results[idx] = text
        # print(dict_of_numerated_parse_results)
        if row[1] == dict_of_numerated_parse_results[0]:
            number_of_correct_parse_on_first_place += 1
        if row[1] in dict_of_numerated_parse_results.values():
            number_of_correct_parses += 1
    print("number_of_inputs:          " + str(f'{number_of_inputs:10}'))
    print("semantics accuracy:        " + str(f'{100/number_of_inputs * number_of_correct_parse_on_first_place:10.2f}'))
    print("semantics oracle accuracy: " + str(f'{100/number_of_inputs * number_of_correct_parses:10.2f}'))


if __name__ == '__main__':

    FOLDER_FOT_ANOTATED_EXAMPLES = "anotated_samples"
    FILENAME_OF_ANOTATED_EXAMPLES = "annotated_diesl_queries"
    tb1_grammar = Grammar(collect_all_rules_from_csv_files())

    # print(get_testset(FOLDER_FOT_ANOTATED_EXAMPLES, FILENAME_OF_ANOTATED_EXAMPLES))

    check_testset(FOLDER_FOT_ANOTATED_EXAMPLES, FILENAME_OF_ANOTATED_EXAMPLES)