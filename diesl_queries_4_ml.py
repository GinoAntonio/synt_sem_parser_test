import random
# A 100%
from collections import namedtuple

DieslQuery = namedtuple("DieslQuery", ["intent", "snippet"])


manualDieslQueries = [
    DieslQuery("whitespace right", "db.student.name = db.student.name.trim(right)"),
    DieslQuery("trim whitespace left", "db.student.name = db.student.name.trim(left)"),

    DieslQuery('"Hello" to operation',  '"Hello".toOperation'),
    DieslQuery("make 'Hello' to operation", '"Hello".toOperation'),
    DieslQuery('to upper this', 'db.student.new = db.student.old.toUpper()'),
    DieslQuery('to lower "word"', 'db.student.new = db.student.old.toLower()'),
    DieslQuery('replace "hello" for "word"',
               'db.student.new. = db.student.old.replace("hello".toOperation, "word".toOperation)'),
    DieslQuery('replace hello for "word"',
               'db.student.new. = db.student.old.replace("hello".toOperation, "word".toOperation)')
]

random.shuffle(manualDieslQueries)

trainingManualQueries = \
    manualDieslQueries[int(len(manualDieslQueries) * 0.1):]

testingManualQueries = \
    manualDieslQueries[:int(len(manualDieslQueries) * 0.1)]

queryName = "small_diesl_queries"
