"""Module containing handcrafted ot queries with nim semantics for testing/training."""

import random
import makeRules
from collections import namedtuple


QtQuery = namedtuple("QtQuery", ["intent", "snippet"])

random.seed(3042330287)  # use seed for reproducable results

manualQtQueries = [
    #
    QtQuery("read Excel", "parseExcel()"),  # import xlsx
    QtQuery("read Excel File", "parseExcel()"),  # import xlsx
    QtQuery("write to csv", "f.toCsv()"),  # which needs import xlsx
    QtQuery("save as csv", "f.toCsv()"),  # which needs import xlsx
    QtQuery("convert to csv", "f.toCsv()"),  # which needs import xlsx

]
#
# random.shuffle(QtQuery)
#
# trainingManualPandasQueries = \
#     QtQuery[int(len(QtQuery) * 0.1):]
#
# testingManualPandasQueries = \
#     QtQuery[:int(len(QtQuery) * 0.1)]
