"""Module containing handcrafted ot queries with dss (domain specific semantics) for testing/training."""

import random
import makeRules
from collections import namedtuple


QtQuery = namedtuple("QtQuery", ["intent", "snippet"])

random.seed(3042330287)  # use seed for reproducable results

manualQtQueries = [
    QtQuery("read Excel", "[[[read][Excel]][source]]"),  # nested version
    QtQuery("read Excel", "[read[Excel][source]]"),  # binary version
    QtQuery("write to csv", "[write[[file][[as][csv]]]"),
    # as we have no trinary trees,
    # we can use a nested semantic (which is here the case)
    # or a binary when making "as" a stop word
    QtQuery("save as csv", "[write[[file][[as][csv]]]"),  # nested semantic
    QtQuery("convert to csv", "[write[[file][[as][csv]]]"),  # nested semantic
]
#
# random.shuffle(QtQuery)
#
# trainingManualPandasQueries = \
#     QtQuery[int(len(QtQuery) * 0.1):]
#
# testingManualPandasQueries = \
#     QtQuery[:int(len(QtQuery) * 0.1)]
