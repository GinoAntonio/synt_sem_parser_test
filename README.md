# sem_synt_pars_evaluate_test



for TB1 operations

**Grammar**

to add or edit grammar:

- the fields are: lhs;rhs;semantic
    lhs(left hand side);rhs(right hand side) ;semantic
- just edit one of the csv files or
- add a new one in the folder "rules collection"
- "#"- the hash sign can be used at a beginning of a line to skip that line, 
  when making comments
- if a line is empty it will be skipped as well
- the delimiter is ";"-semicolon sign



**annotated queries**

the annotated diesl queries:
- the fields are: input;output
    input by user; handcrafted correct semantic output
- the delimiter is ";"-semicolon sign
- "#"- the hash sign can be used at a beginning of a line to skip that line, 
  when making comments
- if a line is empty it will be skipped as well
