from collections import defaultdict

from annotators import *
from rule import Rule
from types import FunctionType
from copy import copy
from functools import cached_property

from ot_parser import apply_lexical_rules, apply_unary_rules, apply_binary_rules, apply_annotators

annotatorList = TokenAnnotator()

# 60% G 40% R
def is_cat(label):
    return label.startswith('$')


def is_lexical(rule):
    return all([not is_cat(rhsi) for rhsi in rule.rhs])


def is_unary(rule):
    """
    Returns true iff the given Rule is a unary compositional rule, i.e.,
    contains only a single category (non-terminal) on the RHS.
    """
    return len(rule.rhs) == 1 and is_cat(rule.rhs[0])


def is_binary(rule):
    return len(rule.rhs) == 2 and is_cat(rule.rhs[0]) and is_cat(rule.rhs[1])


def is_optional(label):
    """
    Returns true iff the given RHS item is optional, i.e., is marked with an
    initial '?'.
    """
    return label.startswith('?') and len(label) > 1


def contains_optionals(rule):
    """Returns true iff the given Rule contains any optional items on the RHS."""
    return any([is_optional(rhsi) for rhsi in rule.rhs])


def add_rule_containing_optional(grammar, rule):
    """
    Handles adding a rule which contains an optional element on the RHS.
    We find the leftmost optional element on the RHS, and then generate
    two variants of the rule: one in which that element is required, and
    one in which it is removed.  We add these variants in place of the
    original rule.  (If there are more optional elements further to the
    right, we'll wind up recursing.)

    For example, if the original rule is:

        Rule('$Z', '$A ?$B ?$C $D')

    then we add these rules instead:

        Rule('$Z', '$A $B ?$C $D')
        Rule('$Z', '$A ?$C $D')
    """
    # Find index of the first optional element on the RHS.
    first = next((idx for idx, elt in enumerate(rule.rhs) if is_optional(elt)), -1)
    assert first >= 0
    assert len(rule.rhs) > 1, 'Entire RHS is optional: %s' % rule
    prefix = rule.rhs[:first]
    suffix = rule.rhs[(first + 1):]
    # First variant: the first optional element gets deoptionalized.
    deoptionalized = (rule.rhs[first][1:],)
    add_rule(grammar, Rule(rule.lhs, prefix + deoptionalized + suffix, rule.sem))
    # Second variant: the first optional element gets removed.
    # If the semantics is a value, just keep it as is.
    sem = rule.sem
    # But if it's a function, we need to supply a dummy argument for the removed element.
    if isinstance(rule.sem, FunctionType):
        sem = lambda sems: rule.sem(sems[:first] + [None] + sems[first:])
    add_rule(grammar, Rule(rule.lhs, prefix + suffix, sem))


def add_n_ary_rule(grammar, rule):
    """
    Handles adding a rule with three or more non-terminals on the RHS.
    We introduce a new category which covers all elements on the RHS except
    the first, and then generate two variants of the rule: one which
    consumes those elements to produce the new category, and another which
    combines the new category which the first element to produce the
    original LHS category.  We add these variants in place of the
    original rule.  (If the new rules still contain more than two elements
    on the RHS, we'll wind up recursing.)
    For example, if the original rule is:
        Rule('$Z', '$A $B $C $D')
    then we create a new category '$Z_$A' (roughly, "$Z missing $A to the left"),
    and add these rules instead:
        Rule('$Z_$A', '$B $C $D')
        Rule('$Z', '$A $Z_$A')
    """

    def add_category(base_name):
        assert is_cat(base_name)
        name = base_name
        while name in grammar.categories:
            name = name + '_'
        grammar.categories.add(name)
        return name

    category = add_category('%s_%s' % (rule.lhs, rule.rhs[0]))
    add_rule(grammar, Rule(category, rule.rhs[1:], lambda sems: sems))
    add_rule(grammar, Rule(rule.lhs, (rule.rhs[0], category),
                           lambda sems: apply_semantics(rule, [sems[0]] + sems[1])))


def add_rule(grammar, rule):
    if contains_optionals(rule):
        add_rule_containing_optional(grammar, rule)
    elif is_lexical(rule):
        grammar.lexical_rules[rule.rhs].append(rule)
    elif is_unary(rule):
        grammar.unary_rules[rule.rhs].append(rule)
    elif is_binary(rule):
        grammar.binary_rules[rule.rhs].append(rule)
    elif all([is_cat(rhsi) for rhsi in rule.rhs]):
        add_n_ary_rule(grammar, rule)
    else:
        # One of the exercises will ask you to handle this case.
        raise Exception('RHS mixes terminals and non-terminals: %s' % rule)


def apply_semantics(rule, sems):
    # Note that this function would not be needed if we required that semantics
    # always be functions, never bare values.  That is, if instead of
    # Rule('$E', 'one', 1) we required Rule('$E', 'one', lambda sems: 1).
    # But that would be cumbersome.
    if isinstance(rule.sem, FunctionType):
        return rule.sem(sems)
    else:
        return rule.sem


class Grammar:
    def __init__(self, rules=[], annotators=[annotatorList], start_symbol='$ROOT'):
        # TODO: figure out if we need a starting symbol
        self.categories = set()
        self.lexical_rules = defaultdict(list)
        self.unary_rules = defaultdict(list)
        self.binary_rules = defaultdict(list)
        self.annotators = annotators
        self.start_symbol = start_symbol
        for rule in rules:
            add_rule(self, rule)
        print('Created grammar with %d rules.' % len(rules))

    def parse(self, sentence: str):
        """Returns a list of parses for the given sentence."""
        # Unterteilen den satz in wörter, erstellen chart, wenden alle regele, annotator...
        # am ende eine kategorie
        tokens = sentence.split()
        chart = defaultdict(list)  # map from span (i, j) to list of parses
        for j in range(1, len(tokens) + 1):
            for i in range(j - 1, -1, -1):
                apply_annotators(chart, tokens, i, j)
                apply_lexical_rules(chart, tokens, i, j)

                apply_binary_rules(chart, i, j)
                apply_unary_rules(chart, i, j)

        parses = chart[(0, len(tokens))]

        # remove parses that do not have the expected start symbol as semantic root (if defined)
        if self.startSymbol is not None:
            parses = [parse for parse in parses if parse.rule.lhs == self.startSymbol]
        return parses

    def parse_input(self, input):
        """Returns a list of parses for the given input."""
        return self.parse_input(self, input)

    @cached_property
    def _rules(self):
        """A list of all rules contained by this grammar."""
        return [rule for rules in self.lexical_rules.values() for rule in rules] + \
               [rule for rules in self.unary_rules.values() for rule in rules] + \
               [rule for rules in self.binary_rules.values() for rule in rules] + \
               self.annotators

    @property
    def rules(self):
        """
        A list of all rules contained in the grammar.

        Note that changes to this list do not add/remove rules from this grammar.
        """
        # NOTE: do not return the cached property itself, since the returned object may be
        # manipulated by the caller, resulting in inaccurate values for later calls
        return copy(self._rules)

    @cached_property
    def _ruleFeatureDict(self):
        ruleFeatureDict = dict()

        # single rules
        for rule in self.rules:
            ruleFeatureDict[str(rule)] = 0

        # combined rules (aka rule precedences)
        for rule1 in self.rules:
            for rule2 in self.rules:
                ruleFeatureDict[(str(rule1), str(rule2))] = 0

        return ruleFeatureDict

    @property
    def ruleFeatureDict(self):
        """
        A dictionary mapping string representation of rules to floats with value 0.

        This dictionary is intended to be used for the generation of feature vectors for parse
        scoring. It maps the string representation of single rules as well as tuples of
        the string combinations of binary combinations of rules to floats (which are
        initially set to 0).
        Note that this property returns a new copy of the dictionary for every call.
        """
        # NOTE: do not return the cached property itself, since the returned object may be
        # manipulated by the caller, resulting in inaccurate values for later calls
        return copy(self._ruleFeatureDict)
