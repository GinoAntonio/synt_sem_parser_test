from rule import Rule

# 80% A- 20% G

def sems_0(sems):
    return sems[0]


def sems_1(sems):
    return sems[1]


def merge_dicts(d1, d2):
    if not d2:
        return d1
    result = d1.copy()
    result.update(d2)
    return result


def get_project():
    project = "db"
    return project


def get_table():
    table = "student"
    return table


def get_diesl_start():
    # TODO: disover how to use the previous used functions to print without brackets
    """ The starting of most of the diesl operations is the same,
    that is way we need this function that returns the starting of most of the parses"""

    return "db.student"


def get_toOperation():
    return ".toOperation"


rules_ot = [
    Rule('$ROOT', '$DieslOperation', sems_0),
    Rule('$DieslOperation', '$OperatorWithTwoArguments', sems_0),
    Rule('$DieslOperation', "$Refactor", sems_0),
    Rule('$DieslOperation', "$OperationWithArgument", sems_0)

]

###################################################################################################

lexical_rules = [
    # TODO: make a rule that selects everything between "" and makes it to a token
    # TODO: figure out why our lexical rules need two things in the definition
    #  and why if there are 2 thing the second is not recognized

    Rule('$toOperation', 'operation', 'operation'),
    Rule('$toOperation', 'Operation', 'Operation'),

    Rule('$Replace', 'Replace', 'Replace'),
    Rule('$Replace', 'replace', 'replace'),

    Rule('$Remove', 'Remove', 'Remove'),
    Rule('$Remove', 'remove', 'remove'),

    Rule("$For", "for", "for"),

    Rule("$Comma", ",", ","),
    Rule("$Space", " ", " "),

    Rule("$HashKey", "#", "#"),
    Rule("$Number", "1", lambda sems: (sems[0])),
    Rule("$Number", "2", lambda sems: (sems[0])),
]

##############################################################################################

unary_rules = [

    Rule("$Argument", "$Number", "value"),
    Rule("$Argument", "$Token", sems_0),
    Rule("$Argument", "$Character", sems_0),
]

###############################################################################################

operator_rules = [
    # TODO: by $Refactor, if there are ""  already, cut them out, otherwise put the argument in ""

    Rule("$Refactor", "$toOperation $Argument", lambda sems: (sems[1], ".", "toOperation")),
    Rule("$Refactor", "$Argument $toOperation", lambda sems: (sems[0], ".", "toOperation")),
    Rule("$Refactor", "$toOperation $Argument", lambda sems: (sems[1], ".", "toOperation")),

    Rule("$OperationWithArgument", "$Remove" "$Argument" "$Argument", lambda sems: (sems[1], ".", "remove")),

    # The apply_binary_rule allows to divide into substrings so for example $Argument $Argument
    # can be a $OperatorWithTwoArguments, (we don't need/want this)
    Rule('$OperatorWithTwoArguments', "$Replace" "$Argument",
         (lambda sems: (str(get_diesl_start()), ".new.", sems[0], " = ",
                        str(get_diesl_start()), ".old.", sems[0],
                        "(", sems[1], str(get_toOperation()), ", ", sems[2], str(get_toOperation()), ")"))),


]

###############################################################################################

rules_optionals = [
    Rule('$OTQueryElement', '$OTQueryElement $Optionals', sems_0),
    Rule('$OTQueryElement', '$Optionals $OTQueryElement', sems_1),


    Rule('$Optional', '$Stopword'),
    Rule('$Optional', '$Determiner'),

    Rule('$Stopword', 'all'),
    Rule('$Stopword', 'of'),
    Rule('$Stopword', 'what'),
    Rule('$Stopword', 'will'),
    Rule('$Stopword', 'it'),
    Rule('$Stopword', 'to'),

    Rule('$Determiner', 'a'),
    Rule('$Determiner', 'an'),
    Rule('$Determiner', 'the'),
]


def new_rules():
    return rules_ot + lexical_rules + unary_rules + operator_rules + rules_optionals
