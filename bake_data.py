import os
import pickle
from parse_scorer import featureVector, emptyFeatureVector
from grammar import Grammar
from makeRules import MakeRules
from scipy.sparse import csr_matrix
from rules_collection.accumulatorForRules import collect_all_rules_from_csv_files
from diesl_queries_4_ml import trainingManualQueries, testingManualQueries, queryName
import time
from ot_parser import parse_input, parse_semantic
from diesl_rules import diesl_rules
from typing import Any
from sklearn import cluster
from sklearn.tree import DecisionTreeRegressor
from sklearn.cluster import FeatureAgglomeration

SEED = 4244919083  # use random seed for reproducable results

# R
def getResourcePath():
    """Return the path to the used resource folder."""
    return os.path.join(os.path.dirname(__file__), "./resources")


def saveResource(obj: Any, identifier: str):
    """
    Save a resource with the given identifier.

    Resources saved in this way can be loaded using `loadResource`.
    """
    filepath = os.path.join(getResourcePath(), f"{identifier}.p")
    return pickle.dump(obj, open(filepath, "wb"))

def loadResource(identifier: str):
    """
    Load the resource specified by the identifier.

    The identifier is the name of the file without any prefixed path information
    (e.g. directories) and the suffixed file ending of '.p'.
    """
    filepath = os.path.join(getResourcePath(), f"{identifier}.p")

    try:
        obj = pickle.load(open(filepath, "rb"))
    except FileNotFoundError:
        obj = None

    return obj




def timestampPrint(obj):
    """Print the given object and prefix the current time as timestamp."""
    now = time.localtime()
    timestamp = time.strftime("%H:%m:%S", now)
    print(f"[{timestamp}] {obj}")

# A
def generateDieslTrainingVectors(trainingData):
    """Generate training feature vectors and result vector from Diesl intent-snippet-pairs.
    Input: trainingData is a Tuple, which has a NL snippet at the 0th position and the expected result on the 1st.
    Outputs the vector of used rules and a vector which has 0 for not matching parses and 1 for matching parses.
    NOTE: the empty parses are just ignored, since we cannot learn from them.
    """
    tb1_grammar = Grammar(diesl_rules())
    trainingFeatureVectors = []
    trainingResult = []

    # Make the training data to a query
    to_make_query = [query[0] for query in trainingData]
    queries = [MakeRules(query) for query in to_make_query]
    result = [query[1] for query in trainingData]
    annotated_queries = zip(queries, result)

    for entry in queries:
        parses = parse_input(tb1_grammar, entry.input)
        if not parses:
            # Append a feature vector that has all 0s
            print(f"no parses were found for this entry: ")
        for parse in parses:
            trainingFeatureVectors.append(featureVector(parse, tb1_grammar))

    for query, result in annotated_queries:
        semantics = parse_semantic(tb1_grammar, query)
        for item in semantics:
            print("item", item, "result: ", result)
            if item == result:
                trainingResult.append(1)
            else:
                trainingResult.append(0)
    return trainingFeatureVectors, trainingResult

#R with changes of A to adapt
def main():

    scaler = None
    reducer = FeatureAgglomeration()
    model = DecisionTreeRegressor()



    # LOAD TRAININGS DATA
    timestampPrint("Loading datasets...")
    pandasTrainingData = trainingManualQueries
    pandasTestingData = testingManualQueries
    timestampPrint("Done.")

    # MAKE TRAINING AND TESTING FEATURE VECTORS
    timestampPrint("Baking trainings feature vectors...")

    # Generate pandas training vectors
    trainingDieslFeatureVectors, trainingDieslResult = generateDieslTrainingVectors(pandasTrainingData)

    # Making data sparse for persisting purposes
    sparseTrainingsDataTuple = (
        csr_matrix(trainingDieslFeatureVectors),
        csr_matrix(trainingDieslResult)
        )
    saveResource(sparseTrainingsDataTuple, "pandas_training_data_sparse")

    timestampPrint("Done.")

    # CREATE TESTING VECTORS

    timestampPrint("Baking trainings feature vectors...")

    # Generate pandas training vectors
    testingDieslFeatureVectors, testingDieslResult = generateDieslTrainingVectors(pandasTestingData)

    # Making data sparse
    sparseTestingDataTuple = (
        csr_matrix(testingDieslFeatureVectors),
        csr_matrix(testingDieslResult)
    )
    saveResource(sparseTestingDataTuple, "pandas_testing_data_sparse")

    timestampPrint("Done.")

    # TRAINING,
    # NOTE: WE WILL NOT USE SCALERS, SINCE WE ARE USING LITTLE DATA
    resourceName = f"{str(scaler)}_{str(reducer)}_{queryName}"
    timestampPrint("Baking ...")

    if scaler is not None:
        # We will not use scalers but someone might in the future
        scaledTrainingFeatureVectors = scaler.transform(trainingDieslFeatureVectors)
    else:
        scaledTrainingFeatureVectors = trainingDieslFeatureVectors

        reducer.fit(scaledTrainingFeatureVectors, trainingDieslResult)

    # Apply reducer
    reducedTrainingFeatureVectors = reducer.transform(scaledTrainingFeatureVectors)

    # Fit for scoring
    model.fit(reducedTrainingFeatureVectors, trainingDieslResult)

    # Score
    trainingScore = model.score(
        reducedTrainingFeatureVectors,
        trainingDieslResult)

    # Save model
    saveResource(model, resourceName)

    timestampPrint(f"Training Accuracy = {trainingScore}.")


if __name__ == "__main__":
    main()
