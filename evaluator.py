from ot_parser import parse_input
from collections.abc import Iterable

# 70% Alessandro, 30% G
def parse_list(tb1_list, grammar):
    # Returns a list of a list, which has a list of all the possible parses for every example of the given list
    # Example: [[parse0_example 0, parse1_example0],[parse0_example1]]
    list_of_parses_for_all_examples = []
    for example in tb1_list:
        list_of_parses_for_one_example = []
        parses = parse_input(grammar, example.input)
        for parse in parses:
            t = parse.semantics
            if isinstance(t, Iterable):
                text = "".join(t)
                list_of_parses_for_one_example.append(text)
            else:
                list_of_parses_for_one_example.append(t)
        list_of_parses_for_all_examples.append(list_of_parses_for_one_example)
    return list_of_parses_for_all_examples


def evaluate(tb1_list, grammar):
    """ Evaluates the grammar, and returns
        -semantics accuracy: percentage of how many 0th parses of each example match the expected semantics
                             of the example
        -semantics oracle accuracy: percentage of if least one parse matched the expected semantics of the example
    """
    correct_0th_parse = 0
    any_parse_correct = 0
    for example in tb1_list:
        list_of_parses_for_one_example = []
        print()
        print('%-16s %s' % ('input', example.input))
        parses = parse_input(grammar, example.input)
        for parse in parses:
            t = parse.semantics
            if isinstance(t, Iterable):
                text = "".join(t)
                list_of_parses_for_one_example.append(text)
            else:
                list_of_parses_for_one_example.append(t)
        if list_of_parses_for_one_example[0] == example.semantics:
            correct_0th_parse += 1
        if example.semantics in list_of_parses_for_one_example:
            any_parse_correct += 1
    number_of_examples = len(tb1_list)
    semantics_accuracy = correct_0th_parse/number_of_examples
    semantics_oracle_accuracy = any_parse_correct/number_of_examples
    print(f"semantics accuracy: {'%.3f'%semantics_accuracy}")
    print(f"semantics oracle accuracy: {'%.3f'%semantics_oracle_accuracy}")


def evaluate_no_print(queries, result, grammar):
    """ Evaluates the grammar, and returns
        -semantics accuracy: percentage of how many 0th parses of each example match the expected semantics
                             of the example
        -semantics oracle accuracy: percentage of if least one parse matched the expected semantics of the example
    """
    correct_0th_parse = 0
    any_parse_correct = 0
    annotated_queries = zip(queries, result)
    for example, result in annotated_queries:
        list_of_parses_for_one_example = []
        print()
        print('%-16s %s' % ('input', example.input))
        parses = parse_input(grammar, example.input)
        for parse in parses:
            t = parse.semantics
            print("semantics: ", parse.semantics)
            if isinstance(t, Iterable):
                text = "".join(t)
                list_of_parses_for_one_example.append(text)
            else:
                list_of_parses_for_one_example.append(t)
        if len(parses) > 0:
            if list_of_parses_for_one_example[0] == result:
                correct_0th_parse += 1
            if result in list_of_parses_for_one_example:
                any_parse_correct += 1
    number_of_examples = len(queries)
    semantics_accuracy = correct_0th_parse/number_of_examples
    semantics_oracle_accuracy = any_parse_correct/number_of_examples
    print(f"semantics accuracy: {'%.3f'%semantics_accuracy}")
    print(f"semantics oracle accuracy: {'%.3f'%semantics_oracle_accuracy}")



