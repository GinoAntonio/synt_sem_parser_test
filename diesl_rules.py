from grammar import Rule


# Alessandro 90% Gino 10%

def sems_0(sems):
    return sems[0]


def sems_1(sems):
    return sems[1]


def merge_dicts(d1, d2):
    if not d2:
        return d1
    result = d1.copy()
    result.update(d2)
    return result


def get_project():
    project = "db"
    return project


def get_table():
    table = "student"
    return table


def get_diesl_start():
    # TODO: disover how to use the previous used functions to print without brackets
    """ The starting of most of the diesl operations is the same,
    that is way we need this function that returns the starting of most of the parses"""

    return "db.student"


def get_toOperation():
    return ".toOperation"


rules_ot = [
    Rule('$ROOT', '$DieslQuery', sems_0),
    Rule("$DieslQuery", "$Refactor", sems_0),
    Rule("$DieslQuery", "$CompareRel", sems_0),
    Rule("$DieslQuery", "$OperationWithArgument", sems_0),
    Rule("$DieslQuery", "$OperatorWithTwoArguments", sems_0),
    Rule("$DieslQuery", "$TrimWhitespace", sems_0),
    Rule("$DieslQuery", "$OperatorWithArgument", sems_0),


]

###################################################################################################

lexical_rules = [
    # TODO: add rules for semantics of a lexical rule. ex: Maximum has maximum as semantic

    Rule('$toOperation', 'operation', 'operation'),
    Rule('$toOperation', 'Operation', 'Operation'),

    Rule('$Remove', 'Remove', 'Remove'),
    Rule('$Remove', 'remove', 'remove'),

    Rule("$Maximum", "maximum", "maximum"),
    Rule("$Maximum", "Maximum", "Maximum"),

    Rule("$Replace", "replace", "replace"),  # replace(s: string; sub, by: char): string {...}
    Rule("$ReplaceWord", "replace", "replaceWord"),  # replaceWord(s, sub: string; by = ""): string {...}

    Rule("$Left", "left"),
    Rule("$Right", "right"),
    Rule("$Both", "both"),

    Rule("$Whitespace", "whitespace", "whitespace"),
    Rule("$Whitespace", "Whitespace", "Whitespace"),

    Rule("$Lower", "lower", "lower"),
    Rule("$Lower", "Lower", "Lower"),

    Rule("$Upper", "upper", "upper"),
    Rule("$Upper", "Upper", "Upper"),

    Rule("$Trim", "trim", "trim"),
    Rule("$Trim", "trim", "trim"),

    Rule("$Write", "write", "write"),

    Rule("$Change", "change", "change"),

    Rule("$String", "string", "string"),
    Rule("$String", "String", "string"),
    Rule("$Instead", "instead", "instead"),
    Rule("$For", "for", "for"),
    Rule("$Make", "make", "make"),

    Rule("$Comma", ",", ","),
    Rule("$Space", " ", ","),

    Rule("$HashKey", "#", "#"),

    Rule("$Column", "column", sems_0)
]

##############################################################################################

unary_rules = [
    Rule("$Delimiter", "$Comma", ","),
    Rule("$Delimiter", "$Semicolon", ";"),
    Rule("$Delimiter", "$Space", " "),
    Rule("$Delimiter", "$HashKey", "#"),
    Rule("$Command", "$Trim", "trim"),
    Rule("$Command", "$Change", "change"),
    Rule("$Parameter", "$String", "String"),
    Rule("$Argument", "$Number", "value"),
    Rule("$Argument", "$Token", sems_0),
    Rule("$Argument", "$Character", sems_0),
    Rule("$Change", "$Make", sems_0),
    Rule("$Direction", "$Left", "left"),
    Rule("$Direction", "$Right", "right"),
    Rule("$Direction", "$Both", "both"),

    # Rule("$OperatorWithArgument", "$CompareRel", sems_0),
    Rule("$OperationWithArgument", "$Remove", sems_0),

    # -- Adjectives --
    # Rule("$Adjective", "$Lower", "ToLower"),
]

###############################################################################################

operator_rules = [
    Rule("$Refactor", "$toOperation $Argument", lambda sems: (sems[1], ".", "toOperation")),
    Rule("$Refactor", "$Argument $toOperation", lambda sems: (sems[0], ".", "toOperation")),
    Rule("$Refactor", "$toOperation $Argument", lambda sems: (sems[1], ".", "toOperation")),

    Rule("$CompareRel", "$Maximum $Argument", lambda sems: (sems[0], "(", sems[1], ")")),

    Rule("$OperationWithArgument", "$Remove $Argument",
         lambda sems: (str(get_diesl_start()), ".name", " = ",
                       str(get_diesl_start()), ".name.", sems[0],
                       "(", sems[1], ")")),

    Rule("$OperationWithArgument", "$Lower $Argument",
         lambda sems: (str(get_diesl_start()), ".new", " = ",
                       str(get_diesl_start()), ".old.", "toLower",
                       "()")),
    Rule("$OperationWithArgument", "$Upper $Argument",
         lambda sems: (str(get_diesl_start()), ".new", " = ",
                       str(get_diesl_start()), ".old.", "toUpper",
                       "()")),
    Rule("$TrimWhitespace", "$Whitespace $Direction",
         lambda sems: (str(get_diesl_start()), ".name = ",
                       str(get_diesl_start()), ".name.trim", "(", sems[1], ")")),




    Rule('$OperatorWithArgument', "$Command ?$Parameter", lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$OperatorWithArgument', "$Replace $Character", lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$OperatorWithArgument', "$Command ?$Parameter", lambda sems: (sems[0], "(", sems[1], ")")),

    Rule('$SplitOperationWithDelimiter', '$Split $Delimiter', lambda sems: (sems[0], sems[1])),
    Rule('$SplitOperationWithDelimiterInBrackets', '$Split $Delimiter', lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$SplitOperationWithDelimiterInBrackets', '$Split $HashKey', lambda sems: (sems[0], "(", sems[1], ")")),
    Rule('$SplitOperationWithDelimiterInBrackets', '$Split $Comma', lambda sems: (sems[0], "(", sems[1], ")")),

    Rule('$OperatorWithTwoArguments', "$ReplaceCharacter $Character $Character",
         lambda sems: (sems[0], "(", sems[1], ",", sems[2], ")")),

    Rule('$OperatorWithTwoArguments', "$Replace $Argument $Argument",
         lambda sems: (str(get_diesl_start()), ".new.", " = ",
                       str(get_diesl_start()), ".old.", sems[0],
                       "(", sems[1], str(get_toOperation()), ", ", sems[2], str(get_toOperation()), ")")),

    Rule('$OperatorWithTwoArguments', "$Command $Parameter $Adjective",
         lambda sems: (sems[0], "(", sems[1], ",", sems[2], ")")),
    Rule('$OperatorWithTwoArguments', "$Command $Parameter $Adjective",
         lambda sems: (sems[1], ".", sems[2])),
]

###############################################################################################

rules_optionals = [
    Rule('$OTQueryElement', '$OTQueryElement $Optionals', sems_0),
    Rule('$OTQueryElement', '$Optionals $OTQueryElement', sems_1),

    Rule('$Optionals', '$Optional ?$Optionals'),

    Rule('$Optional', '$Stopword'),
    Rule('$Optional', '$Determiner'),

    Rule('$Stopword', 'all'),
    Rule('$Stopword', 'of'),
    Rule('$Stopword', 'what'),
    Rule('$Stopword', 'will'),
    Rule('$Stopword', 'it'),
    Rule('$Stopword', 'to'),

    Rule('$Determiner', 'a'),
    Rule('$Determiner', 'an'),
    Rule('$Determiner', 'the'),
]


def diesl_rules():
    return rules_ot + lexical_rules + unary_rules + operator_rules + rules_optionals
